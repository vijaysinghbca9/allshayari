package com.asysinfo.allshayari;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by vijay.k on 29-01-2018.
 */

public class ShayariDatabase {

    private static final String TAG = "ShayariDatabase";
    private static final String DATABASE_NAME = "ALL_SHAYARI";

    public static final String COL_SHAYARI_TYPE_ID = "SHAYARITYPEID";
    public static final String COL_SHAYARI_TYPE = "SHAYARITYPE";
    public static final String COL_SHAYARI_ID = "SHAYARIID";
    public static final String COL_SHAYARI = "SHAYARI";
    public static final String COL_SHAYARI_STATUS_ID = "SHAYARSTATUSID";
    public static final String COL_SHAYARI_STATUS = "SHAYARISTATUS";
    public static final String COL_SHAYARI_USER_IMEI = "USER_IMEI";
    public static final String COL_SHAYARI_FAVORITES = "FAVORITES";

    private static final String ASYS_ALL_SHAYARI = "ASYS_ALL_SHAYARI";
    private static final String ASYS_SHAYARI_TYPES = "ASYS_SHAYARI_TYPES";
    private static final String ASYS_SHAYARI_STATUS = "ASYS_SHAYARI_STATUS";
    private static final String ASYS_SHAYARI_FAVORITE = "ASYS_SHAYARI_FAVORITE";

    private static final int DATABASE_VERSION = 1;
    private final DatabaseOpenHelper mDatabaseOpenHelper;

    public ShayariDatabase(Context context) {
        mDatabaseOpenHelper = new DatabaseOpenHelper(context);
    }

    private static class DatabaseOpenHelper extends SQLiteOpenHelper {

        private final Context mHelperContext;
        private SQLiteDatabase mDatabase;

        private static final String SHAYARI_TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + ASYS_ALL_SHAYARI +
                        " USING fts3 (" +
                        COL_SHAYARI_ID + ", " +
                        COL_SHAYARI_TYPE_ID + ", " +
                        COL_SHAYARI + ", " +
                        COL_SHAYARI_STATUS_ID + ")";

        private static final String SHAYARI_TYPE_TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + ASYS_SHAYARI_TYPES +
                        " USING fts3 (" +
                        COL_SHAYARI_TYPE_ID + ", " +
                        COL_SHAYARI_TYPE + ")";

        private static final String SHAYARI_STATUS_TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + ASYS_SHAYARI_STATUS +
                        " USING fts3 (" +
                        COL_SHAYARI_STATUS_ID + ", " +
                        COL_SHAYARI_STATUS + ")";

        private static final String SHAYARI_FAVORITES_TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + ASYS_SHAYARI_FAVORITE +
                        " USING fts3 (" +
                        COL_SHAYARI_USER_IMEI + ", " +
                        COL_SHAYARI_FAVORITES + ")";

        DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mHelperContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            mDatabase = db;
            mDatabase.execSQL(SHAYARI_TABLE_CREATE);
            mDatabase.execSQL(SHAYARI_TYPE_TABLE_CREATE);
            mDatabase.execSQL(SHAYARI_STATUS_TABLE_CREATE);
            mDatabase.execSQL(SHAYARI_FAVORITES_TABLE_CREATE);
            try {
                loadShayariIntoDB();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + ASYS_ALL_SHAYARI);
            onCreate(db);
        }

        private void loadShayariIntoDB() {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        loadShayariTypes();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }).start();
        }

        private void loadShayari(String shayariTypeId, String shayariType) {
            int arrayId = mHelperContext.getResources()
                    .getIdentifier(shayariType + "_shayari", "array", mHelperContext.getPackageName());
            for (String shayari: mHelperContext.getResources().getStringArray(arrayId)) {

                ContentValues initialValues = new ContentValues();
                initialValues.put(COL_SHAYARI_TYPE_ID, shayariTypeId);
                initialValues.put(COL_SHAYARI, shayari);
                initialValues.put(COL_SHAYARI_STATUS_ID, "1");
                long id =  mDatabase.insert(ASYS_ALL_SHAYARI, null, initialValues);
            }
        }

        private void loadShayariTypes() throws IOException {
            final Resources resources = mHelperContext.getResources();
            InputStream inputStream = resources.openRawResource(R.raw.shayari_types);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] strings = TextUtils.split(line, "~");
                    if (strings.length < 2) continue;

                    ContentValues initialValues = new ContentValues();
                    String shayaryTypeId = strings[0].trim();
                    String shayaryType = strings[1].trim();
                    initialValues.put(COL_SHAYARI_TYPE_ID, shayaryTypeId);
                    initialValues.put(COL_SHAYARI_TYPE, shayaryType);
                    long id =  mDatabase.insert(ASYS_SHAYARI_TYPES, null, initialValues);

                    if (id < 0) {
                        Log.e(TAG, "unable to add shayari type: " + strings[0].trim());
                    }
                    loadShayari(shayaryTypeId, shayaryType);
                }
            } finally {
                reader.close();
            }
        }
    }

    public Cursor getShayariMatches(String query, String[] columns) {
        String selection = COL_SHAYARI + " MATCH ?";
        String[] selectionArgs = new String[] {"*" + query + "*"};

        return query(selection, selectionArgs, columns);
    }

    @Nullable
    private Cursor query(String selection, String[] selectionArgs, String[] columns) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ASYS_ALL_SHAYARI);

        Cursor cursor = builder.query(mDatabaseOpenHelper.getReadableDatabase(),
                columns, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public Cursor getFavoriteShayaris(String imei) {
        String selection = COL_SHAYARI_USER_IMEI + " MATCH ?";
        String[] selectionArgs = new String[] {"*" + imei + "*"};
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ASYS_SHAYARI_FAVORITE);

        Cursor cursor = builder.query(mDatabaseOpenHelper.getReadableDatabase(),
                null, selection, selectionArgs, null,
                null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public long insertFavoriteShayari(String imei, String shayari) {
        if (shayariExistsInFavoriteList(shayari)) {
            return -1;
        }
        else {
            ContentValues favorite = new ContentValues();
            favorite.put(COL_SHAYARI_USER_IMEI, imei);
            favorite.put(COL_SHAYARI_FAVORITES, shayari);
            return mDatabaseOpenHelper.getReadableDatabase()
                    .insert(ASYS_SHAYARI_FAVORITE, null, favorite);
        }
    }

    public void removeFavoriteShayari(String imei, String shayari) {
        long id =  mDatabaseOpenHelper.getReadableDatabase()
                .delete(ASYS_SHAYARI_FAVORITE, COL_SHAYARI_USER_IMEI
                        + "=? AND " + COL_SHAYARI_FAVORITES
                        + "=?", new String[]{ imei, shayari });
    }

    private boolean shayariExistsInFavoriteList(String shayari) {
        boolean result = false;
        Cursor cursor = null;
        try {
            String selection = COL_SHAYARI_FAVORITES + " MATCH ?";
            String[] selectionArgs = new String[]{"*" + shayari + "*"};
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(ASYS_SHAYARI_FAVORITE);

            cursor = builder.query(mDatabaseOpenHelper.getReadableDatabase(),
                    null, selection, selectionArgs, null,
                    null, null);

            if (cursor.getCount() > 0) result = true;
        } catch (SQLiteException e) {
            throw e;
        } finally {
            if (cursor != null) cursor.close();
        }
        return result;
    }
}
