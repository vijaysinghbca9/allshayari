package com.asysinfo.allshayari;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static Toolbar toolbar;
    public static ListView listView;
    public static View listViewRow;
    public static FloatingActionButton fab_share;
    public static FloatingActionButton fab_favorite;
    public static FloatingActionButton fab_delete;
    public static int mSelectedPosition;
    private static int optionsItemSelected;
    public static SharedPreferences sharedPref;
    private CharSequence shayari;
    private ShayariDatabase shayariDatabase;
    private TelephonyManager telephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));

        if (shayariDatabase == null)
            shayariDatabase = new ShayariDatabase(this);

        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        fab_share = (FloatingActionButton) findViewById(R.id.fab_share);
        fab_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareShayari();
            }
        });
        fab_share.hide();

        fab_favorite = (FloatingActionButton) findViewById(R.id.fab_favorite);
        fab_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToFavorites();
            }
        });
        fab_favorite.hide();

        fab_delete = (FloatingActionButton) findViewById(R.id.fab_delete);
        fab_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeFromFavorite();
            }
        });
        fab_delete.hide();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        PreferenceManager.setDefaultValues(this, R.xml.pref_notification, false);
        PreferenceManager.setDefaultValues(this, R.xml.pref_data_sync, false);
        PreferenceManager.setDefaultValues(this, R.xml.pref_font_size, false);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.edit().putInt(SettingsActivity.KEY_LIST_PREF_FONT_SIZE, 12);

        constructShayariListView();
        handleSerachIntent(getIntent());
        AppRater.app_launched(this);

        /*Toast.makeText(this, sharedPref.getString(
                SettingsActivity.KEY_LIST_PREF_FONT_SIZE, "1"), Toast.LENGTH_SHORT).show();*/
    }

    private void constructShayariListView() {
        listView = (ListView) findViewById(R.id.shayariListView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                // TODO Auto-generated method stub
                if (mSelectedPosition != pos && listViewRow != null)
                    listViewRow.setBackgroundResource(R.drawable.shayari_burnt_box);

                listViewRow = view;
                listView.setSelection(pos);
                mSelectedPosition = pos;

                TextView textShayari = (TextView) view.findViewById(R.id.textShayari);
                if (textShayari != null)
                    shayari = textShayari.getText();
                else
                    return true;
                Log.v("long clicked", "pos: " + pos + "shayari: " + shayari);

                view.setBackgroundResource(R.drawable.row_selector);
                fab_share.show();
                if(optionsItemSelected == R.id.action_favorites)
                    fab_delete.show();
                else
                    fab_favorite.show();

                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mSelectedPosition != position && listViewRow != null) {
                    fab_share.hide();
                    fab_favorite.hide();
                    fab_delete.hide();
                    listViewRow.setBackgroundResource(R.drawable.shayari_burnt_box);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        inflater.inflate(R.menu.menu_search, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        optionsItemSelected = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (optionsItemSelected == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (optionsItemSelected == R.id.action_favorites) {
            showFavorites();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        try {
            int id = item.getItemId();
            AllShayariListAdapter allShayariListAdapter = null;
            View mainFrameView = findViewById(R.id.mainFrameLayout);

            fab_share.hide();
            fab_delete.hide();
            fab_favorite.hide();
            optionsItemSelected = 0;

            if (id == R.id.nav_love) {
                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.love_shayari));
                toolbar.setTitle("Love Shayari");
            } else if (id == R.id.nav_sad) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.sad_shayari));
                toolbar.setTitle("Sad Shayari");

            } else if (id == R.id.nav_painfull) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.painfull_shayari));
                toolbar.setTitle("Painfull Shayari");

            } /*else if (id == R.id.nav_hindi) {

                // Handle the hindi shayari action
                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.hindi_shayari));
                        toolbar.setTitle("Hindi Shayari");

            } else if (id == R.id.nav_mirja_galib) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.mirja_galib_shayari));
                        toolbar.setTitle("Mirja Galib Shayari");

            }*/ else if (id == R.id.nav_friendship) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.friendship_shayari));
                        toolbar.setTitle("Friendship Shayari");

            } else if (id == R.id.nav_romantic) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.romantic_shayari));
                        toolbar.setTitle("Romantic Shayari");

            }/* else if (id == R.id.nav_helplessness) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.helplessness_shayari));
                        toolbar.setTitle("Helpless Shayari");

            } else if (id == R.id.nav_encouragement) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.encouragement_shayari));
                        toolbar.setTitle("Encouragement Shayari");

            } else if (id == R.id.nav_brother_sister) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.brother_sister_shayari));
                        toolbar.setTitle("Bother Sister Shayari");

            } else if (id == R.id.nav_relationship) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.relationship_shayari));
                        toolbar.setTitle("Relationship Shayari");

            } else if (id == R.id.nav_poor) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.poor_shayari));
                        toolbar.setTitle("Poor Shayari");

            } else if (id == R.id.nav_bye_bye) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.bye_bye_shayari));
                        toolbar.setTitle("Bye Bye Shayari");

            } else if (id == R.id.nav_broken_heart) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.broken_heart_shayari));
                        toolbar.setTitle("Broken Heart Shayari");

            } else if (id == R.id.nav_dream) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.dream_shayari));
                        toolbar.setTitle("Dream Shayari");

            }*/ else if (id == R.id.nav_good_thoughts) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.good_thoughts_shayari));
                        toolbar.setTitle("Good Thoughts Shayari");

            }/* else if (id == R.id.nav_smoking) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.smoking_shayari));
                        toolbar.setTitle("Smoking Shayari");

            } else if (id == R.id.nav_Feelings) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.feelings_shayari));
                        toolbar.setTitle("Feelings Shayari");

            } else if (id == R.id.nav_heart_touching) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.heart_touching_shayari));
                        toolbar.setTitle("Heart Touching Shayari");

            } else if (id == R.id.nav_love_pain) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.love_pain_shayari));
                        toolbar.setTitle("Love & Pain Shayari");

            } else if (id == R.id.nav_night) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.night_shayari));
                        toolbar.setTitle("Night Shayari");

            } else if (id == R.id.nav_breakup) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.breakup_shayari));
                        toolbar.setTitle("Breakup Shayari");

            } else if (id == R.id.nav_life) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.life_shayari));
                        toolbar.setTitle("Life Shayari");

            } else if (id == R.id.nav_time) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.time_shayari));
                        toolbar.setTitle("Time Shayari");

            } else if (id == R.id.nav_tears) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.tears_shayari));
                        toolbar.setTitle("Tears Shayari");

            } else if (id == R.id.nav_excellent) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.excellent_shayari));
                        toolbar.setTitle("Excellent Shayari");

            } else if (id == R.id.nav_hatred) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.hatred_shayari));
                        toolbar.setTitle("Hatred Shayari");

            } else if (id == R.id.nav_adult) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.adult_shayari));
                        toolbar.setTitle("Adult Shayari");

            } else if (id == R.id.nav_childhood) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.childhood_shayari));
                        toolbar.setTitle("Childhood Shayari");

            } else if (id == R.id.nav_new) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.new_shayari));
                        toolbar.setTitle("New Shayari");

            } else if (id == R.id.nav_guilt) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.guilt_shayari));
                        toolbar.setTitle("Guilt Shayari");

            } else if (id == R.id.nav_wait) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.wait_shayari));
                        toolbar.setTitle("Wait Shayari");

            } else if (id == R.id.nav_betrayal) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.betrayal_shayari));
                        toolbar.setTitle("Betrayal Shayari");

            } else if (id == R.id.nav_trust) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.trust_shayari));
                        toolbar.setTitle("Trust Shayari");

            } else if (id == R.id.nav_morning) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.morning_shayari));
                        toolbar.setTitle("Morning Shayari");

            } else if (id == R.id.nav_beautiful) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.beautiful_shayari));
                        toolbar.setTitle("Beautiful Shayari");

            } else if (id == R.id.nav_praise) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.praise_shayari));
                        toolbar.setTitle("Praise Shayari");

            } else if (id == R.id.nav_fate) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.fate_shayari));
                        toolbar.setTitle("Fate Shayari");

            } else if (id == R.id.nav_desire) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.desire_shayari));
                        toolbar.setTitle("Desire Shayari");

            } else if (id == R.id.nav_silent) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.silent_shayari));
                        toolbar.setTitle("Silent Shayari");

            } else if (id == R.id.nav_parting) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.parting_shayari));
                        toolbar.setTitle("Parting Shayari");

            } else if (id == R.id.nav_best) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.best_shayari));
                        toolbar.setTitle("Best Shayari");

            } else if (id == R.id.nav_funny) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.funny_shayari));
                        toolbar.setTitle("Funny Shayari");

            } else if (id == R.id.nav_loneliness) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.loneliness_shayari));
                        toolbar.setTitle("Loneliness Shayari");

            } else if (id == R.id.nav_heart) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.heart_shayari));
                        toolbar.setTitle("Heart Shayari");

            } else if (id == R.id.nav_wish) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.wish_shayari));
                        toolbar.setTitle("Wish Shayari");

            } else if (id == R.id.nav_bewafa) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.bewafa_shayari));
                        toolbar.setTitle("Bewafa Shayari");

            } else if (id == R.id.nav_himorher) {

                mainFrameView.setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                allShayariListAdapter = new AllShayariListAdapter(this,
                        getResources().getStringArray(R.array.him_or_her_shayari));
                        toolbar.setTitle("Him & Her Shayari");

            }*/ else if (id == R.id.nav_share) {

                mainFrameView.setBackgroundResource(R.drawable.shayari_watermark);
                this.shareAllShayariApp();

            } else if (id == R.id.nav_rate_us) {

                try {
                    mainFrameView.setBackgroundResource(R.drawable.shayari_watermark);
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(getResources().getString(R.string.app_google_play_link))));
                } catch (Exception ex) {
                    Log.e("Error", "An error occurred while rating us.", ex);
                }

            } else if (id == R.id.nav_feedback) {

                mainFrameView.setBackgroundResource(R.drawable.shayari_watermark);
                sendFeedback();
            }

            this.listView.setAdapter(allShayariListAdapter);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } catch (Exception ex) {
            Log.e("Error", "An unknown error occurred.", ex);
        }
        return true;
    }

    private void shareShayari() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shayari.toString());
        sendIntent.setType("text/plain");
        try {
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_via)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, ex.getMessage() + "Or app is not installed.",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
        if (listViewRow != null)
            listViewRow.setBackgroundResource(R.drawable.shayari_burnt_box);
        fab_share.hide();
        fab_favorite.hide();
        fab_delete.hide();
    }

    private void shareAllShayariApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "All Shayari");
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "\nLet me recommend you the best shayari application I found.\n"
                        + getResources().getText(R.string.app_google_play_link));
        sendIntent.setType("text/plain");
        try {
            startActivity(Intent.createChooser(sendIntent,
                    getResources().getText(R.string.share_via)));
        } catch (Exception ex) {
            Toast.makeText(this, "Error: " + ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean sendFeedback() {
        Intent Email = new Intent(Intent.ACTION_SEND);
        Email.setType("text/email");
        Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@asysinfo.com"});
        Email.putExtra(Intent.EXTRA_SUBJECT, "All Shayari: Feedback");
        Email.putExtra(Intent.EXTRA_TEXT, "Team\nAll Shayari," + "");
        startActivity(Intent.createChooser(Email, "Send Feedback:"));
        return true;
    }

    private void addToFavorites() {
        try {
            long id = shayariDatabase.insertFavoriteShayari(telephonyManager.getDeviceId(),
                    shayari.toString());
            if (id > 0) {
                Toast.makeText(this, "Added to your favorite list.",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Shayari already exists in your favorite list.",
                        Toast.LENGTH_SHORT).show();
            }
        }  catch (SecurityException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        if (listViewRow != null)
            listViewRow.setBackgroundResource(R.drawable.shayari_burnt_box);
        fab_share.hide();
        fab_favorite.hide();
        fab_delete.hide();
    }

    private void showFavorites() {
        Cursor cursor = null;
        try {
            String imei = telephonyManager.getDeviceId();

            cursor = shayariDatabase.getFavoriteShayaris(imei);

            if (cursor != null && cursor.moveToFirst()) {
                String[] shayariList = new String[cursor.getCount()];
                int index = 0;

                do {
                    shayariList[index++] = cursor.getString(cursor
                            .getColumnIndex(ShayariDatabase.COL_SHAYARI_FAVORITES));
                } while (cursor.moveToNext());

                findViewById(R.id.mainFrameLayout).setBackgroundColor(getResources()
                        .getColor(R.color.colorMainBackground));
                this.listView.setAdapter(new AllShayariListAdapter(this, shayariList));
            } else {
                this.listView.setAdapter(null);
                optionsItemSelected = 0;
                Toast.makeText(this, "Your favorite list is empty.",
                        Toast.LENGTH_SHORT).show();
            }
            toolbar.setTitle("My Favorites");

        } catch (SecurityException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
    }

    private void removeFromFavorite() {
        try {
            shayariDatabase.removeFavoriteShayari(telephonyManager.getDeviceId(),
                    shayari.toString());
            Toast.makeText(this, "Removed from your favorite list.",
                    Toast.LENGTH_SHORT).show();
        }  catch (SecurityException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            showFavorites();
        }
        if (listViewRow != null)
            listViewRow.setBackgroundResource(R.drawable.shayari_burnt_box);
        fab_share.hide();
        fab_favorite.hide();
        fab_delete.hide();
    }

    private void handleSerachIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            Cursor cursor = shayariDatabase.getShayariMatches(query, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    String[] shayariList = new String[cursor.getCount()];
                    int index = 0;

                    do {
                        shayariList[index++] = cursor.getString(cursor
                                .getColumnIndex(ShayariDatabase.COL_SHAYARI));
                    } while (cursor.moveToNext());

                    findViewById(R.id.mainFrameLayout).setBackgroundColor(getResources()
                            .getColor(R.color.colorMainBackground));
                    this.listView.setAdapter(new AllShayariListAdapter(this, shayariList));
                } else {
                    Toast.makeText(this, "No matching shayari found.",
                            Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) cursor.close();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        TextView textShayari = (TextView) findViewById(R.id.textShayari);
        if (textShayari != null) {
            textShayari.setTextSize(Integer.parseInt(MainActivity
                    .sharedPref.getString(SettingsActivity.KEY_LIST_PREF_FONT_SIZE, "12")));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleSerachIntent(intent);
    }
}
