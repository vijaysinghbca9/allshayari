package com.asysinfo.allshayari;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vijay.k on 15-01-2018.
 */

public class AllShayariListAdapter extends ArrayAdapter {

    private final Activity context;
    public String[] shayaris;
    private static boolean proVersion;
    Bitmap bitmap;
    AdView adViewNew;

    public AllShayariListAdapter(@NonNull Activity context, String[] shayaris) {
        super(context, R.layout.allshayari_list_row, shayaris);
        this.context = context;
        this.shayaris = shayaris;
        bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.burn_background);

        adViewNew = new AdView(context);
        adViewNew.setAdSize(AdSize.LARGE_BANNER);
        adViewNew.setAdUnitId(context.getString(R.string.banner_ad_unit_id));
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        boolean showAd = proVersion == false && ((position + 1) % 7 == 0);

        if (showAd) {
            adViewNew.loadAd(new AdRequest.Builder().build());
            return adViewNew;
        } else {
            view = inflater.inflate(R.layout.allshayari_list_row, null, true);

            final TextView textShayari = (TextView) view.findViewById(R.id.textShayari);
            if (textShayari != null) {
                textShayari.setText(this.shayaris[position]);
                textShayari.setTextSize(Integer.parseInt(MainActivity
                        .sharedPref.getString(SettingsActivity.KEY_LIST_PREF_FONT_SIZE, "12")));

                /*view.post(new Runnable() {
                    @Override
                    public void run() {
                        int height = textShayari.getMeasuredHeight();
                        int bitmapHeight = bitmap.getHeight();
                        if (height > bitmapHeight) {
                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap,
                                    height, bitmap.getWidth(), true);
                            textShayari.setCompoundDrawablesWithIntrinsicBounds(null, null, null, new BitmapDrawable(resizedBitmap));
                        }
                    }
                });*/
            }
        }
        /*if (position == MainActivity.mSelectedPosition && MainActivity.listViewRow != null) {
            MainActivity.listViewRow.setBackgroundResource(R.drawable.row_selector);
            MainActivity.fab_share.show();
        }*/

        int posDiff = position - MainActivity.mSelectedPosition;
        if (posDiff < 0) posDiff *= -1;
        if (posDiff >= 2) {
            MainActivity.fab_share.hide();
            MainActivity.fab_favorite.hide();
            MainActivity.fab_delete.hide();
        }
        return view;
    }
}
